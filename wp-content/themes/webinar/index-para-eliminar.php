<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage  Tema_Webinar
 * @since  Tema Webinar 1.0
 */
 ?>
 <?php get_header(); ?>
 <a href="<?php echo get_site_url().'/labarra'; ?>">LaBarrra</a>
  <a href="<?php echo get_site_url().'/ialimentos'; ?>">IAlimentos</a>
   <a href="<?php echo get_site_url().'/fierros'; ?>">Fierros</a>
    <a href="<?php echo get_site_url().'/enobra'; ?>">EnObra</a>
<?php get_footer(); ?>
</body>

