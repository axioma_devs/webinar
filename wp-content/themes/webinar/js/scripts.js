jQuery.noConflict()

function registrado(){
    jQuery('.btn-clock').addClass('hidden');
    jQuery('#formRegistro').addClass('hidden');
    // jQuery('#wb-caledar').addClass('hidden');
    jQuery('.url-webinar').removeClass('hidden');
  }
jQuery(document).ready(function(jQuery) {

// INICIO CONTEO //

      var dateTimeSplit = jQuery('#dateTime').val().split(' ');
      var dateSplit = dateTimeSplit[0].split('/');
      var Split = dateTimeSplit[2];
      var timeSplit = dateTimeSplit[1].split(':');
      var hora = timeSplit[0];
      if (Split=='pm' && hora!=12) {
        hora= parseInt(timeSplit[0]) +12;
      }else{

        hora= timeSplit[0];
      }

      var hoy = new Date();
      var dd = hoy.getDate();
      jQuery('#dia').val(parseInt(dateSplit[2])-parseInt(dd)+1);

      var currentDate = dateTimeSplit[0] + ' ' + hora +  ':' + timeSplit[1];

    jQuery.noConflict();
      jQuery('.time-clock').countdown(currentDate) /*parametro que coloca la fecha final del conteo*/
        .on('update.countdown', function(event) {
            var format = ''; /*%D days %H:%M:%S* formato para visualizar full time colocar dentro de la variable format*/
            if (event.offset.seconds >= 0) {
                format = '<p class="format">' + '%-S' + '<span class="format-text">' + ' Seg' + '%!S' + '</span>' + '</p>' + format;
            }
            if (event.offset.minutes >= 0) {
                format = '<p id="min" class="format">' + '%-M' + '<span class="format-text">' + ' Min' + '%!M' + '</span>' + '</p>' + format;
            }
            if (event.offset.hours >= 0) {
                format = '<p id="hora" class="format">' + '%-H' + '<span class="format-text">' + ' Hora' + '%!H' + '</span>' + '</p>' + format;
            }
            if (event.offset.days >= 0) {
                format = '<p id="dia" class="format style="display:block;">' + '%-D' + '<span class="format-text">' + ' Día' + '%!D' + '</span>' + '</p>' + format;
            }

            jQuery(this).html(event.strftime(format));
        })
        .on('finish.countdown', function(event) {
            jQuery(this).html('<div class="end">' + 'Nuestra video conferencia ha comenzado' + '</div>')
                .parent().addClass('disabled');

        });

// FIN CONTEO //

// INICIO LOAD AJAX //
jQuery(document).ajaxStart(function() {
    jQuery('.tab-loader').addClass('loading');
    jQuery('.tab-content').css('display','block');
});

jQuery(document).ajaxComplete(function() {
    jQuery('.tab-loader').removeClass('loading');
    jQuery('.tab-content').css('display','none');
});

// FIN LOAD AJAX //

// INICIO ANIMACION //
arrayAnimate = new Array();

jQuery('.btn-form').click(function() {
  arrayAnimate[0]=(jQuery("#sectionForm"));
  jQuery('html, body').animate({
    scrollTop: arrayAnimate[0].offset().top -50
  }, 500);
  return false;
})

jQuery('.btnexpo').click(function() {
  arrayAnimate[1]=(jQuery("#exponentes"));
  jQuery('html, body').animate({
    scrollTop: arrayAnimate[1].offset().top
  }, 500);
  return false;
})

// INICIO ANIMACION //

var iCnt = 0;

jQuery('#btEliminar').click(function() {
  if (iCnt==1)
  {
  	jQuery("#btEliminar").addClass('hidden');
  	jQuery("#btEnviarInv").addClass('hidden');
  	jQuery("#contentRegistro").css("height","16%");
  	jQuery('#btInvitar').val('Invitar');
  }
  jQuery("#invitado"+iCnt).remove();
  iCnt=iCnt-1;
})

jQuery('#btInvitar').click(function() {

  jQuery("#btEnviarInv").removeClass('hidden');
  jQuery("#btEliminar").removeClass('hidden');
  jQuery('#btInvitar').val('Invitar Mas');
  jQuery("#contentRegistro").css("height","42%");

	var validate_form = false;
  var invitado = jQuery("#invitados").last();

    if (iCnt>0) {
	    if(invitado.find('#nombre'+iCnt+'').val() != '' && invitado.find('#nombre'+iCnt+'').val() != ''){
	      validate_form = true;
	    }else{
	      alert('Llenar los campos E-MAIL... y NOMBRE');
	    }
	}else{
		validate_form = true;
	}

  if (iCnt>=0 && validate_form) {
    iCnt++;
    jQuery("#invitados").append('<div class="\invitado\" id="\invitado'+iCnt+'\"><li><label class="titlePerson">Persona '+iCnt+'</label><label>Nombre</label><input name="nombreInv[]" class="nombreInv" id=\"nombre'+iCnt+'\"placeholder="Nombre" type="text"></li><li><label>E-mail</label><input name="emailInv[]" class="emailInv" type="text" placeholder="E-mail"></li></div>');
}
})

function showErrorForms(data) {
  try {
    objArray = JSON && JSON.parse(data) || jQuery.parseJSON(data);
  } catch(e) {
    alert('Hubo un error con nuestro servicio, favor intente más tarde');
    console.log(data);
    console.log('\n');
    console.log(e);
    return false;
  }

  arrayKeys = Object.keys(objArray);
  arrayError = new Array();

  jQuery.each(arrayKeys, function(index, val) {
    jQuery("#" + val).html(objArray[val]);
    objArray[val] != "" && arrayError.push(jQuery("#" + val));
  });
}

 jQuery(document).on('submit', '#formInvitados', function() {
	var post_url = ajax_object.ajaxInv_url;
  	var data = jQuery('#formInvitados').serialize();
    jQuery.ajax({
      type: 'POST',
      url: post_url,
      data:data,
      success: function(data) {
      	var nombres = new Array();
      	if (data == 1) {
        jQuery('.spanExitoso').text('');
      	var i=1;
      	while( i<=(iCnt))
        {
      			valor =jQuery('#nombre'+i+'').val();
            nombre
      			[i-1]=valor;
      			i++;
      	}
      	 jQuery("#contentMensaje").css("height","10%");
         jQuery('#form-invitacion .fancybox-close-small').trigger('click');
         setTimeout(function(){
        if(!jQuery("#form-invitacion").is(':visible'))
          {
            jQuery('#mensaje').trigger('click');
          }
        }, 500);

        console.log(valor);
        jQuery("#contMensaje").append('<span class="spanExitoso">Usted ha invitado a '+nombres+' asistir a este evento.</span>');

        }else if (data!="") {
          // alert(data);
        }

      },
      error: function(data) {
        alert('Error de conexion, Favor verifique su conexión a internet');
      }
    });
    return false;
  });

  jQuery(document).on('submit', '#formRegistro', function() {
    var nombre = jQuery('#nombre').val();
    jQuery('#nombreReg').val(nombre);
  	var post_url = my_ajax_object.ajax_url;
  	var data = jQuery(this).serialize();
    jQuery.ajax({
      type: 'POST',
      url: post_url,
      data:data,
      success: function(data) {
        iCnt = 0;
      	var nombres;
      	if (data == 'registro_exitoso') {
          var dia = parseInt(jQuery('#dia').val());
          var canal =jQuery('#canal').val();
          Cookies.set('webinar-axioma', canal, {expires: dia ,path: '/'});
          jQuery('#invitacion').trigger('click');
      		jQuery('#formRegistro .formInput').val('');
      		jQuery( ".invitado" ).remove();
      		jQuery('#btInvitar').val('Invitar');
        	jQuery("#contentRegistro").css("height","16%");
	        jQuery("#btEliminar").addClass('hidden');
	      	jQuery("#btEnviarInv").addClass('hidden');
          jQuery('.form-error').text('');

          registrado();

        }else if (data=="registro_error") {
          var dia = parseInt(jQuery('#dia').val());
          Cookies.set('webinar-axioma', 'true', {expires: dia ,path: '/'});
          registrado();

        }else{
          showErrorForms(data);
        }
        console.log(data);
      },
      error: function(data) {
        alert('Error de conexion, Favor verifique su conexión a internet');
      }
    });
    return false;
  });


  if (jQuery('#sectionOrg').find('li').length == 1 ) {
      jQuery('#sectionOrg').find('li').addClass('exponentes-margin');
  }

})

jQuery(document).on('click', '#check-terminos', function() {
  return false;
})
