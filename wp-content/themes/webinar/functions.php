<?php
add_theme_support( 'post-thumbnails');

if ( ! function_exists('themewich_theme_styles') ) :
	/**
	 * Loads themes stylesheets
	 *
	 * @since Edition 1.0
	 */
	function themewich_theme_styles () {

		wp_register_style( 'portal-style', get_template_directory_uri() . '/style.css' );
		wp_register_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.min.css' );
		
		wp_enqueue_style( 'portal-style' );
		wp_enqueue_style( 'fancybox' );
	}
	add_action('wp_enqueue_scripts', 'themewich_theme_styles');
endif;


if ( ! function_exists('themewich_register_js') ) :
	/**
	 * Registers and loads front-end javascript
	 *
	 * @since Edition 1.0
	 */
	function themewich_register_js() {

			// wp_register_script('migrate', get_template_directory_uri() . '/js/jquery-migrate-3.0.0.min.js', 'jquery', true);
			wp_register_script('jquery','https://code.jquery.com/jquery-2.2.3.min.js', 'jquery', true);
			wp_register_script('countdown', get_template_directory_uri() . '/js/jquery.countdown.js', 'jquery', true);		
			wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js?1.1', 'jquery', true);	

			wp_register_script('fancybox', get_template_directory_uri() . '/js/jquery.fancybox.min.js', 'jquery', true);	

			wp_localize_script( 'scripts', 'my_ajax_object',
           	array( 'ajax_url' => get_template_directory_uri().'/formulario.php' ));

           	wp_localize_script( 'scripts', 'ajax_object',
           	array( 'ajaxInv_url' => get_template_directory_uri().'/formularioInv.php' ));

           	wp_register_script( 'cookie', get_template_directory_uri(). '/js/js.cookie.js', true);

			$variables_array = array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'get_template_directory_uri' => get_template_directory_uri(),
				'nonce' => wp_create_nonce('ajax-nonce')
			);
			wp_localize_script('custom', 'agAjax', $variables_array);


			wp_enqueue_script('jquery');
			wp_enqueue_script('countdown');
			wp_enqueue_script('scripts');
			wp_enqueue_script('custom');
			wp_enqueue_script('fancybox');
			wp_enqueue_script('cookie');
	}
	add_action('wp_enqueue_scripts', 'themewich_register_js');
endif;


add_action( 'add_meta_boxes', 'dynamic_add_custom_box' );
add_action( 'save_post', 'dynamic_save_postdata' );

/* Adds a box to the main column on the Post and Page edit screens */
function dynamic_add_custom_box() {
    add_meta_box(
        'organizadores_id',
        'Organizadores',
        'dynamic_inner_custom_box',
        array('page','labarra',
        'ialimentos',
        'fierros',
        'enobra'));
}

/* Prints the box content */
function dynamic_inner_custom_box() {
    global $post;
    ?>
    <style>
    	.axm_organizador_editor table {
		    width: 100%
		}
		.axm_organizador_editor input:not([readonly]):not([type='button']),
		.axm_organizador_editor textarea {
		    width: 100%
		}
		.axm_organizador_editor img {
		    display: block;
		    max-width: 150px;
		    max-height: 105px
		}
		.axm_organizador_editor .remove {
		    color: #0073aa;
		    text-decoration: underline;
		    cursor: pointer;
		    left: calc(100% - 50px);
		    position: relative
		}
		.axm_organizador_editor .add {
		    color: #0073aa;
		    text-decoration: underline;
		    cursor: pointer;
		    display: block;
		    width: 130px
		}
		.axm_organizador_editor .remove:hover,
		.axm_organizador_editor .add:hover {
		    color: #00a0d2
		}
    </style>

    <div id="meta_inner" class="axm_organizador_editor">
    <?php
	    $organizadores = get_post_meta($post->ID,'organizadores',true);
	    $c = 0;

	    if ($organizadores && count( $organizadores ) > 0 ) {
	        foreach( $organizadores as $organizador ) {
	            if ( isset( $organizador['podcast_file'] ) || isset( $organizador['name'] ) || isset( $organizador['description'] )) {

	            	$img_content = $organizador['podcast_file'] != '' ?'<img width="100%" src="'.$organizador['podcast_file'].'" alt="podcast_file-%1$s">' : '';

	                 printf( '<div> <b>Organizador %1$s</b> <table> <tr> <td valign="bottom"> <table> <tr> <td valign="bottom"> <label for="organizadores[%1$s][podcast_file]">Imágen: </label> </td><td width="100&#37;"> %5$s <input type="text" name="organizadores[%1$s][podcast_file]" value="%2$s" class="podcast_file" readonly><input class="upload_image_button" type="button" value="Seleccionar"> </td></tr></table> </td><td><table><tr><td><label for="name">Nombre: </label></td><td width="100&#37;"> <input type="text" name="organizadores[%1$s][name]" value="%3$s" id="name"> </td></tr><tr><td align="right"> <label for="description">Descripción: </label> </td><td width="100&#37;"> <textarea rows="5" name="organizadores[%1$s][description]" id="description" width="100&#37;">%4$s</textarea> </td></tr>   <tr><td><label for="facebook">Facebook: </label></td><td width="100&#37;"> <input type="text" name="organizadores[%1$s][facebook]" value="%6$s" id="facebook"> </td></tr>  <tr><td><label for="linkedin">Linkedin: </label></td><td width="100&#37;"> <input type="text" name="organizadores[%1$s][linkedin]" value="%7$s" id="linkedin"> </td></tr>  <tr><td><label for="twitter">Twitter: </label></td><td width="100&#37;"> <input type="text" name="organizadores[%1$s][twitter]" value="%8$s" id="twitter"> </td></tr></table> </td></tr></table> <p class="remove">Eliminar</p></div><hr>', ++$c, $organizador['podcast_file'], $organizador['name'],$organizador['description'],$img_content,$organizador['facebook'],$organizador['linkedin'],$organizador['twitter']);
	            }
	        }
	    }
    ?>

	<span id="here"></span>
	<span class="add">Agregar organizador</span>

	<script>
	    var $ = jQuery.noConflict();
		$(document).ready(function() {
		    var count = <?php echo $c; ?>;
		    $(".add").click(function() {
		        count++;
		        $('#here').before('<div> <b>Organizador ' + count + '</b> <table> <tr> <td valign="bottom"> <table> <tr> <td valign="bottom"> <label for="organizadores[' + count + '][podcast_file]">Imágen: </label> </td><td width="100&#37;"> <input type="text" name="organizadores[' + count + '][podcast_file]"  class="podcast_file" readonly><input class="upload_image_button" type="button" value="Seleccionar"> </td></tr></table> </td><td><table><tr><td><label for="name">Nombre: </label></td><td width="100&#37;"> <input type="text" name="organizadores[' + count + '][name]"  id="name"> </td></tr><tr><td align="right"> <label for="description">Descripción: </label> </td><td width="100&#37;"> <textarea rows="5" name="organizadores[' + count + '][description]" id="description" width="100&#37;"></textarea> </td></tr>  <tr><td><label for="facebook">Facebook: </label></td><td width="100&#37;"> <input type="text" name="organizadores[' + count + '][facebook]"  id="facebook"> </td></tr>  <tr><td><label for="linkedin">Linkedin: </label></td><td width="100&#37;"> <input type="text" name="organizadores[' + count + '][linkedin]"  id="linkedin"> </td></tr><tr><td><label for="twitter">Twitter: </label></td><td width="100&#37;"> <input type="text" name="organizadores[' + count + '][twitter]"  id="twitter"> </td></tr></table> </td></tr></table> <p class="remove">Eliminar</p></div>');

		        $('#here div:last').after('<hr>');
		        return false;
		    });

		    $(".remove").live('click', function() {
		        console.log($(this));
		        $(this).parent().next('hr').remove();
		        $(this).parent().remove();
		    });

		    var file_frame, upload_btn;

		    $(document).on('click', '.upload_image_button', function(podcast) {
		        podcast.preventDefault();
		        upload_btn = $(this);

		        if (file_frame) { // If the media frame already exists, reopen it.
		            file_frame.open();
		            return;
		        }
		        // Create the media frame.
		        file_frame = wp.media.frames.file_frame = wp.media({
		            title: $(this).data('uploader_title'),
		            button: {
		                text: $(this).data('uploader_button_text'),
		            },
		            multiple: false // Set to true to allow multiple files to be selected
		        });

		        // When a file is selected, run a callback.
		        file_frame.on('select', function() {
		            attachment = file_frame.state().get('selection').first().toJSON(); // We set multiple to false so only get one image from the uploader
		            var url = attachment.url; // here are some of the variables you could use for the attachment;
		            upload_btn.siblings('.podcast_file').val(url);

		            if (upload_btn.siblings('img').length < 1)
		                upload_btn.closest('td').prepend('<img src="' + url + '" alt="postcast" width="100%">');
		            else
		                upload_btn.siblings('img:first').attr('src', url);;
		        });

		        file_frame.open(); // Finally, open the modal
		    });
		});
    </script>
</div><?php

}

/* When the post is saved, saves our custom data */
function dynamic_save_postdata( $post_id ) {
    // verify if this is an auto save routine. 
    // If it is our form has not been submitted, so we dont want to do anything
     if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

    if ( 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }

    $organizadores = $_POST['organizadores'];
    update_post_meta($post_id,'organizadores',$organizadores);
}


function verifcar_http_url($url){
	return preg_match("/(http:\/\/)/", $url, $output) ? $url : 'http://'.$url;
}