<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>
<?php get_header(); 
include "../../../wp-config.php";
?>

<div class="error404">
	<div class="errortxt">
		<h1>Error 404</h1><br>
		<h2>Página no encontrada</h2><br>
		<?php
			global $wpdb;
			$query_select ="SELECT post_name,post_type FROM `wp_posts` where post_date=(SELECT max(post_date) FROM `wp_posts` WHERE post_type in('ialimentos','labarra','fierros','enobra'))";
			$result_select= $wpdb->get_results( $query_select, OBJECT );
			foreach ($result_select as $value) {
				$url = 'http://axioma.com.co/webinar/'.$value->post_type.'/'.$value->post_name;
			}
			echo "<a href=$url class='btn404'>Ir a la página</a>";
		?>

		
		
	</div>
</div>

<?php get_footer(); ?>
