<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage  Tema_Webinar
 * @since  Tema Webinar 1.0
 */
?>

<?php get_header(); ?>


<!-- hero -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="section">

	<div id="sectionInf">
		<div id="logo">
			<?php
			   if (has_post_thumbnail()) {
			       the_post_thumbnail();
			   }?>
		</div>
		<h1 class="title"><?php the_title(); ?></h1>
		<div class='parrafo'>
			<?php the_content(); ?>
		</div>	 <br>
	<div id="btcta"><a class="btnexpo" href="#exponentes">Exponentes</a></div>
</div>

	<div id="sectionForm">
			<div  class="url-webinar hidden">
				<h2 class="vivo">Bienvenido</h2>
				<div><a class="btnform1" href="<?php echo (get_field('url_webinar')); ?> " target="_blank">Ingresar</a></div>
			</div>
		<form id="formRegistro"   method="POST">
			<div class="col1-form">
			<h2>Registro</h2>
			<li class="li">
				<input type="text" name="nombre" id="nombre" class="formInput" placeholder="Nombre*">
				<span id="nombreErr" class="form-error"></span>
			</li>
			<li class="li">
				<input type="text" name="email" id="email" class="formInput" placeholder="E-mail*">
				<span id="emailErr" class="form-error"></span>
			</li>
			<li class="li">
				<input type="text" name="telefono" id="telefono" class="formInput" placeholder="Teléfono*">
				<span id="telefonoErr" class="form-error"></span>
			</li>
			<li class="li">
				<input type="text" name="empresa" id="empresa" class="formInput" placeholder="Empresa*">
				<span id="empresaErr" class="form-error"></span>
			</li>
			<li class="li">
				<input type="text" name="ciudad" id="ciudad" class="formInput" placeholder="Ciudad*" >
				<span id="ciudadErr" class="form-error"></span>
			</li>
			<?php $post=get_post( $post);?>
			<input type="hidden" name="canal" id="canal" class="formInput" value="<?php echo $post->post_type;?>">

			<input type="hidden" name="campana" id="campana" class="formInput" value="<?php the_title();?>">

			<input type="hidden" name="grupo" id="grupo" class="formInput" value="<?php the_field("group"); ?>">
			<label ><input type="checkbox" id="check-terminos" checked="checked">Acepto <a class="terminos" href="#">términos</a> y <a class="terminos" href="#">condiciones</a></label>
			<input type="submit" value="Enviar" id="btEnviar">
				</div>
		</form>
	</div>
</div>

<?php endwhile;?>
<?php else: ?>
<?php endif; ?>

	<div id="sectionDateTime" class="sectionDatos">
		<div id="sectionDate">
			<input id="dateTime" type="text" value="<?php the_field('fecha'); ?>">
		</div>
		<div class="faltan">
			<h2>
				Faltan
			</h2>
		</div>
		<div class="countdown">
	    	<h2 class="clock time-clock"></h2>
	  	</div>

			<div id="btcta" class="btn-clock"><a class="btnunase btn-form" href="#formRegistro">Unase</a></div>

	  	<div  class="url-webinar hidden"><a class="btnunase" href="<?php echo (get_field('url_webinar')); ?>" target="_blank">Ingresar</a></div>

		 <?php if (get_field('calendario_outlook') || get_field('calendario_google') || get_field('calendario_icalendar')): ?>
	    <div id="wb-caledar">
			 <h3 class="guardar">¡Guardar este evento en su calendario!</h3>
			 <div class="btncalendar">

		    <?php if ( get_field('calendario_outlook')): ?>
		    	 <a href="<?php echo (get_field('calendario_outlook')); ?>" target="_blank" class="btcta">Outlook</a>
		    <?php endif ?>
		    <?php if ( get_field('calendario_google')): ?>
		    	 <a href="<?php echo (get_field('calendario_google')); ?>" target="_blank" class="btcta">Google Calendar</a>
		    <?php endif ?>
		    <?php if ( get_field('calendario_icalendar')): ?>
		    	 <a href="<?php echo (get_field('calendario_icalendar')); ?>" target="_blank" class="btcta">iCalendar</a>
		    <?php endif ?>
				</div>
			</div>
		<?php endif ?>
		</div>

    </div>

<div id="exponentes">
	<div id="sectionOrg" class="sectionDatos">
	<?php
		$post_ID = get_post();
		$post_ID = $post_ID->ID;
		$organizadores_array = get_post_meta($post_ID, 'organizadores')[0];
		if ($organizadores_array != '') {
			foreach ($organizadores_array as $key => $organizador) {
				echo "<li>";
				echo "<img src='".$organizador['podcast_file']."' alt='".$organizador['name']."'>";
				echo "<div class='infOrg'>";
				echo "<label class='nombreOrg'>".$organizador['name']."</label>";
				echo "<label class='descriptionOrg'>".$organizador['description']."</label>";
				echo "<div id='redes-sociales'>";
				if ($organizador['twitter']!='') {
					echo "<a class='redes' href=".verifcar_http_url($organizador['twitter'])."><img class='img-social' src='http://localhost/webinar/wp-content/uploads/2017/06/twitter.png'></a> ";
				}
				if ($organizador['linkedin']!='') {
					echo "<a class='redes' href=".verifcar_http_url($organizador['linkedin'])."><img class='img-social' src='http://localhost/webinar/wp-content/uploads/2017/06/linkedin.png'></a> ";
				}
				if ($organizador['facebook']!='') {
					echo "<a class='redes' href=".verifcar_http_url($organizador['facebook'])."><img class='img-social' src='http://localhost/webinar/wp-content/uploads/2017/06/face-1.png'></a></div>";
				}
				echo "</div>";

				echo "</li>";
			}
		}
	?>
	<p><?php echo get_post_meta(get_the_id(), 'Información', true); ?></p>
	</div>
</div>


	<input type="hidden" name="dia" id="dia">


	<div class="tab-loader"></div>
	<div class="tab-content"></div>

	<a href="#alert-fecha" data-fancybox id="enlace-fecha"></a>

	<script type="text/javascript">
		jQuery("[data-fancybox]").fancybox({
			// Options will go here
		});
	</script>

	<div id="alert-fecha" style="display: none;">
			<div id="contExitoso">
				<p class="pop-text">Faltan:</p>
				<input id="dateTime" type="text" value="<?php the_field('fecha'); ?>">
						<div class="countdown">
	    	<h2 class="time-clock ligthbox-clock"></h2>
	  	</div>
			</div>
	</div>

	<a href="#form-invitacion" data-fancybox id="invitacion"></a>

	<script type="text/javascript">
		jQuery("[data-fancybox]").fancybox({
			// Options will go here
		});
	</script>

	<div id="form-invitacion" style="display: none;">
			<div id="contExitoso">
				<img id="check" src="<?php bloginfo('template_url'); ?>/images/check1.png" alt="">
				<p class="pop-text">Usted se ha registrado exitosamente.</p>
				<p class="pop-text">Quiere invitar a una persona.</p>
			</div>
			<form method="POST" id="formInvitados">
				<div id="invitados"></div>
				<input type="text" id="nombreReg" name="nombreReg" class="hidden">
				<input type="hidden" name="webinar" value="<?php the_title(); ?>">
				<input type="hidden" name="url" value="<?php the_permalink(); ?>">
				<input type="button"  class="bt btnunase" id="btInvitar" value="Invitar" />
				<input type="button" class="bt hidden btnunase" id="btEliminar" value="Eliminar" />
				<input type="submit" class="bt hidden btnunase" id="btEnviarInv" value="Enviar"/>
			</form>
	</div>

	<a href="#form-mensaje" data-fancybox id="mensaje"></a>
	<script type="text/javascript">
		jQuery("[data-fancybox]").fancybox({
			// Options will go here
		});
	</script>

	<div id="form-mensaje" style="display: none; background: rgb(219, 226, 239)">
		<div id='cont'>
				<img id="check" src="<?php bloginfo('template_url'); ?>/images/check1.png" alt="">
				<div id="contMensaje"></div>
			</div>
	</div>

</div>

<?php
	date_default_timezone_set('America/Bogota');
	$current_date = strtotime(date('Y/m/d g:i:s a'));
	$webinar_date = strtotime(get_field('fecha'));
	$dif_date =  $webinar_date - $current_date;
	// echo $dif_date;

	// echo $dif_date;

if((isset($_COOKIE['webinar-axioma']) && $_COOKIE['webinar-axioma']== $post->post_type && $dif_date > (20)) || (isset($_COOKIE['webinar-axioma']) &&  $_COOKIE['webinar-axioma']== $post->post_type && $dif_date <= (950) && $dif_date> (20))){
?>
		<script>
		console.log('cookie y mayor el tiempo - cookie y menor 15 min');
		jQuery('.btn-clock').addClass('hidden');
    	jQuery('#formRegistro').addClass('hidden');
    	// jQuery('#wb-caledar').addClass('hidden');
    	jQuery('.url-webinar').removeClass('hidden');
		</script>

<?php
	}else if(isset($_COOKIE['webinar-axioma']) &&  $_COOKIE['webinar-axioma']== $post->post_type && $dif_date < (20)){
?>
		<script>
		console.log('cookie y menor el tiempo');
		jQuery('.btn-clock').addClass('hidden');
    	jQuery('#formRegistro').addClass('hidden');
    	jQuery('#wb-caledar').addClass('hidden');
    	jQuery('.faltan').addClass('hidden');
    	jQuery('.url-webinar').removeClass('hidden');
		</script>
<?php
	}else if ($dif_date < (20)) { ?>
		<script>
			console.log('menor el tiempo');
			jQuery('#wb-caledar').addClass('hidden');
			jQuery('.faltan').addClass('hidden');
		</script>
	<?php }?>



<?php if ($dif_date > (950)) {?>
	<script>
		jQuery('.url-webinar').click(function() {
			jQuery(".url-webinar").attr('href', '#');
			jQuery('#enlace-fecha').trigger('click');
			return false;
		})
	</script>

<?php
}
?>



<?php get_footer();?>
