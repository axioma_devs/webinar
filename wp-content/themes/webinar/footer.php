<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage  Tema_Webinar
 * @since  Tema Webinar 1.0
 */
?>

<?php wp_footer(); ?>
</body>
</html>
