<?php
global $wp_query;
  $wp_query->set_404();
  status_header( 404 );
  get_template_part( 404 ); exit();
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage  Tema_Webinar
 * @since  Tema Webinar 1.0
 */

get_header(); ?>
<div id="container">
	<div id="content" role="main">

		<?php the_post(); ?>
		<h1 class="entry-title">
			<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
		</h1>

	</div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>
