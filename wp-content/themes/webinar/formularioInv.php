<?php session_start();
// include 'modelo.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	$nombreInv = $_POST['nombreInv'];
	$emailInv = $_POST['emailInv'];
	$nombreReg = $_POST['nombreReg'];
	$url = $_POST['url'];
	$webinar = $_POST['webinar'];

	$hasError = false;

	for($x=0; $x<count($_POST['nombreInv']); $x++)
	{
		if ($nombreInv[$x]=='' && $emailInv[$x]=='') {
			$hasError = true;
			$outputArray="Llenar los campos E-MAIL y NOMBRE";
		}else if (!filter_var($emailInv[$x], FILTER_VALIDATE_EMAIL)) {
			$hasError = true;
			$outputArray = "Formato de correo ".$emailInv[$x]." inválido";
		}
	}

	if(!$hasError){
		echo "1";

	for($x=0; $x<=count($_POST['nombreInv']); $x++)
	{
			$email_to = $emailInv[$x];
			$email_subject = "Webinar";
			$email_from = "ihincapie@revistalabarra.com";
			$time = time();
			
			$email_message ="<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='UTF-8'>
	<title>Document</title>
</head>
  <body >
    <div style='background-color:#112d4e'>
        <table style='background-color:#112d4e' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>
          <tbody>
            <tr>
              <td>
                <table style='margin:0 auto;background-color:#112d4e' width='600' cellspacing='0' cellpadding='0' border='0' align='center'>
                  <tbody>
                    <tr>
                      <td align='center'>
                        <table style='background-color:#112d4e' width='600' cellspacing='0' cellpadding='0' border='0'>
                          <tbody>
                            <tr>
                              <td align='center'>
                                <table style='width:600px;text-align:left;padding-top:0px;background-color:#112d4e' cellspacing='0' cellpadding='0'>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table cellspacing='0' cellpadding='0'>
                                          <tbody>
                                            <tr>
                                              <td>
                                                <table style='border-spacing:0px' width='600' cellspacing='0' cellpadding='0' border='0'>
                                                  <tbody>
                                                    <tr>
                                                      <td style='line-height:0px;padding:20px 0px 10px 0px;' align='center'>
                                                          </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td style='padding:0px'>
                                                <table width='590' cellspacing='0' cellpadding='0'>
                                                  <tbody>
                                                    <tr>
                                                      <td style='vertical-align:top;' width='590'>
                                                      	<div style='width:600px; height:300px; background-image: url(http://axioma.com.co/webinar/wp-content/uploads/2017/07/bg-1.jpg);'>
                                                      	<div style='padding: 15% 0px'>
                                                         <h1 style='text-align:center; color: white;margin: 5px;    font-family: Helvetica;'>Webinar</h1>
                                                         <p  style='text-align:center; color: white;font-size:20px;margin: 5px;    font-family: Helvetica;'>$webinar</p>
                                                         </div>
                                                         </div>
                                                         
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>

                                            <tr>
                                              <td style='padding-bottom: 33px;'>
                                                <table style='background-color:#FFF; text-align: center;' width='600'>
                                                  <tbody>
                                                    <tr><td style='padding:40px 30px 55px 30px;vertical-align:top; text-align: center;' align='left'>
                                                      <div style='word-wrap:break-word;width:100%'>
                                                        <div role='textbox'>
                                                          <p style='font-family:Helvetica;line-height:160%;margin:0px 0px 0px;word-wrap:break-word;color#111;padding-bottom: 20px; text-align: center;'>
                                                            <span style='color:#111;font-size:20px;padding-bottom:10px;font-style:italic;'>
                                                              <strong>Hola, $nombreInv[$x]</strong>
                                                            </span>
                                                             <br>
                                                            <span style='color:#111;font-size:17px; '>
                                                               $nombreReg lo ha invitado hacer parte del webinar.<br> 
                                                            </span>
                                                            
                                                          </p>
                                                          <a style='color:white; background: #112d4e; padding: 10px 40px; border-radius: 50px;text-decoration: none;width: 80px;     font-family: Helvetica;   ' href='$url' target='_blank'>Registrate</a>
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr></tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td align='left'> </td>
                            </tr>
                            <tr>
                              <td align='left'> </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </body>
</html>";


			$headers = 'From: '.$email_from."\r\n".
			'Reply-To: '.$email_from."\r\n" .
			$headers .= "CC: info@axioma.com.co.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

			@mail($email_to, $email_subject, $email_message, $headers);	
	}		
	}else{
		$output = json_encode($outputArray);
		echo $outputArray;	
		}	
	}

?>


