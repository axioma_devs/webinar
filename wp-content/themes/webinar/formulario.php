<?php session_start();
include 'EmblueJSON.php';
include "../../../wp-config.php";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	$updateMode = false;
	$nombre = $_POST['nombre'];
	$email = $_POST['email'];
	$telefono = $_POST['telefono'];
	$empresa = $_POST['empresa'];
	$ciudad = $_POST['ciudad'];
	$canal = $_POST['canal'];
	$campaña = $_POST['campana'];
	$grupo = $_POST['grupo'];

	$hasError = false;
	$outputArray = Array("nombreErr" => "",
						"emailErr"=>"",
						"telefonoErr" => "",
						"empresaErr" => "",
						"ciudadErr"=>"");

	$email = preg_replace('/\s+/', '', $email);

	if($email == ''){
		$hasError = true;
		$outputArray["emailErr"] = "Correo electrónico es requerido";
	}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$hasError = true;
		$outputArray["emailErr"] = "Formato de correo inválido";
	}

	if($nombre == ''){
		$hasError = true;
		$outputArray["nombreErr"] = "Nombre es requerido";
	}elseif (!preg_match("/^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$/i",$nombre)) {
	  $hasError = true;
	  $outputArray["nombreErr"] = "Sólo letras y espacios en blanco son permitidos";
	}

	if($telefono == ''){
		  $hasError = true;
		  $outputArray["telefonoErr"] = "Teléfono es requerido";
	}else if (!preg_match("/[0-9]/",$telefono)) {
		  $hasError = true;
		  $outputArray["telefonoErr"] = "Sólo numeros son permitidos";
	}elseif (!preg_match("/[0-9]{7}/",$telefono)) {
		  $hasError = true;
		  $outputArray["telefonoErr"] = "Numero minimo de 7 digitos";
	}

	if($empresa == ''){
		$hasError = true;
		$outputArray["empresaErr"] = "Empresa es requerido";
	}elseif (!preg_match("/^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$/i",$empresa)) {
	  $hasError = true;
	  $outputArray["empresaErr"] = "Sólo letras y espacios en blanco son permitidos";
	}

	if($ciudad == ''){
		$hasError = true;
		$outputArray["ciudadErr"] = "Ciudad es requerido";
	}


	if(!$hasError)
	{		

		global $wpdb;
		$query_select ="SELECT email FROM `wp_register` WHERE email='$email' and canal='$canal' and campaña='$campaña'";
		$result_select= $wpdb->get_results( $query_select, OBJECT );
		if ($result_select) {
			echo "registro_error";	

		}else{
			$fecha=date("Y-m-d H:i:s");
			$query ="INSERT INTO `wp_register` (canal, campaña, nombre, email, telefono, empresa, ciudad, fecha) VALUES ('$canal','$campaña','$nombre','$email','$telefono','$empresa', '$ciudad', '$fecha')";
			$result= $wpdb->get_results( $query, OBJECT );

			$contactInfo = new Contact($grupo,$nombre,$email,$telefono,$empresa,$ciudad);
			EmblueController::newContact($email);
			$response = EmblueController::newContactFromContact($contactInfo);
			if($response == true)
			{
				echo "registro_exitoso";
			}

			if ($_POST["canal"]=='ialimentos') {

		      $email_to = "ihincapie@revistalabarra.com,mercadeo@revistaialimentos.com";
		      
		    }elseif ($_POST["canal"]=='labarra') {
		    	$email_to = "ihincapie@revistalabarra.com";

		    }elseif ($_POST["canal"]=='enobra') {
		    	$email_to = "ihincapie@revistalabarra.com";

		    }elseif ($_POST["canal"]=='fierros') {
		    	$email_to = "ihincapie@revistalabarra.com";

		    }

		    $email_subject = "Webinar";
		    $email_from = "info@axioma.com.co";
		    $time = time();
		      
		      $email_message = 
		      "Nombre: ".$_POST["nombre"]."\n".
		      "Email: ".$_POST["email"]."\n".
		      "Teléfono: ".$_POST["telefono"]."\n".
		      "Empresa: ".$_POST["empresa"]."\n".
		      "Ciudad: ".$_POST["ciudad"];

		      $headers = 'From: '.$email_from."\r\n".
		      'Reply-To: '.$email_from."\r\n" .
		      'X-Mailer: PHP/' . phpversion();

		      @mail($email_to, $email_subject, $email_message, $headers);
		}
				

	}else{
		$output = json_encode($outputArray);
		echo $output;	
		}
	}