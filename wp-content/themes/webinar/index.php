<?php
  $wp_query->set_404();
  status_header( 404 );
  get_template_part( 404 ); exit();
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage  Tema_Webinar
 * @since  Tema Webinar 1.0
 */
 ?>
 <?php get_header(); 
 	the_title();
 	echo the_content();
  get_footer(); ?>
</body>

