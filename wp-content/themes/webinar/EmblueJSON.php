<?php 

$emblue= new EmblueController();

class EmblueController
{
	static public $token = "";
	static public $service_url ="http://api.embluemail.com/Services/Emblue3Service.svc/json";
	static public $user = "desarrolladorback@revistalabarra.com";
	static public $pass = "Axioma2017.";
	static public $tokenUser="euv9d9XC-EBwfO-OiJKd-ztUFdB8no1";
	static public $empresaId="848";

	
	//Genera Token Inicial//
    
    public function __construct()
    {
        EmblueController::Authenticate();
    }

     public static function callApiToken($service, $params)
     {
     	$cr =EmblueController::$service_url.'/'.$service;
     	$curl = curl_init($cr);

		$data_string = json_encode($params);                                                                                   
		                                                                                                                     
		$ch = curl_init($cr);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$curl_response = curl_exec($ch);

		if ($curl_response === false) {
		    $info = curl_getinfo($curl);
		    curl_close($curl);
		    die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($curl);
		
		$decoded = json_decode($curl_response);
		if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
		    die('error occured: ' . $decoded->response->errormessage);
		}

		return $decoded;
    }

	//Llama al metodo que genera el token//
	public static function Authenticate(){	
		$params = 
		   array(
				'User'=>EmblueController::$user,
				'Pass'=>EmblueController::$pass,
				'Token'=>EmblueController::$tokenUser
		);	
		$service="Authenticate";
		$response=EmblueController::callApiToken($service,$params);

		$_SESSION['token']=$response->Token;
		return $_SESSION['token'];	
	}
	
	//Revisa la conexion en el sistema de emblue//
	public static function checkConnection(){
		$service="CheckConnection";
		$response=EmblueController::callApiToken($service,"");
		return $response;
	}
	
	// Nuevo Contacto
	public static function newContact($email){
		$service="newContact";
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
				'Email'=>$email,
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;		
	}
	
	
	//Enviar datos del contacto y editarlo//
	public static function editContact($contact){
		$service="EditCustomFieldsOneContact";
		
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
		        'EmailId'=>$contact->emailId,
				'EditedFields'=>$contact->serializeData(),
		   );

		// print_r($params);
		// die();
		   
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}

	public static function SearchGroup($group='')
	{
		$service="SearchGroup";
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
		        'FirstResult'=>'0',
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}

	public static function SearchContact($contact=''){
		$service="SearchContact";
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
		        'Search'=>$contact,
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}
	public static function ListCustomFields(){
		$service="ListCustomFields";
		$params = 
		   array(
		        'Token'=>$_SESSION['token']
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}
	public static function GetCustomFieldsByEmail($EmailId){
		$service="GetCustomFieldsByEmail";
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
		        'EmailId'=>$EmailId,
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}
	
	
	//Envia el nuevo contacto a un grupo del sistema creado en emblue//
	public static function RelatedContactGroups($contact){
		$service="RelatedContactGroups";
		$params = 
		   array(
		        'Token'=>$_SESSION['token'],
		        'EmailId'=>$contact->emailId,
				'SelectGroups'=>$contact->grupo,
				// 'DeselectGroups'=>$contact->remover,
		   );
		$response=EmblueController::callApiToken($service,$params);
		return $response;
	}
	
	public static function newContactFromContact($contact){
		$response = EmblueController::newContact($contact->email);
		$contact->emailId = $response->EmailId;
		EmblueController::editContact($contact);
		EmblueController::RelatedContactGroups($contact);
		
		return true;
	}

	function get_objValProperty($array, $propertyName, $value){
		$output = "Not Found";
		foreach ($array as $key => $aVal) {
			 if($aVal->nombre == $propertyName){
			 		if(property_exists($aVal, $value))
			 			$output = $aVal->$value;
			 		else
			 			$output = "";
			 		break;
			 }
		}
		return $output;
	}
}

class Contact
{
	public $grupo,$nombre,$email, $telefono;
	function __construct($grupo,$nombre,$email,$telefono,$empresa,$ciudad)	{
		$this->grupo = $grupo;
		$this->email = $email;
		$this->nombre = $nombre;
		$this->telefono = $telefono;
		$this->empresa = $empresa;
		$this->ciudad = $ciudad;
	}
	function serializeData(){
		$str="";
		$str.= "nombre:|:".$this->nombre.":|:1|||"; 
		$str.= "telefono_1:|:".$this->telefono.":|:1|||";
		$str.= "empresa:|:".$this->empresa.":|:1|||";
		$str.= "ciudad:|:".$this->ciudad.":|:1|||";
		$str= substr($str,0,count($str)-4);
		return $str;
	}
}

class eFunctions
{
	static function agregarEspacios($str){
		return str_replace(" ","%20",$str);
	}

	public static function print_Fields($array, $fields=""){
		if($fields!=""){
			echo "<table>";
					echo "<tr>";
						foreach ($fields as $key => $header) {
							echo "<td><strong>$header</strong></td>";
						}
					echo "</tr>";

					foreach ($array as $key => $val1) {
						echo "<tr>";
							
							foreach ($fields as $key => $val2) {
								if (property_exists($val1, $val2)) {
									echo "<td style='text-align:left'>".$val1->{$val2}."</td>";
								}
								else{
									echo "<td style='text-align:left'>Property Not Found</td>";
								}
							}
						echo "</tr>";
					}
			echo "</table>";
		}else{
			echo "<table>";
				echo "<tr>";
					foreach ($array[0] as $key => $header) {
						echo "<td><strong>$key</strong></td>";
					}
				echo "</tr>";
				foreach ($array as $key => $val1) {

					echo "<tr>";
					$nombre = 'nombre';
						foreach ($val1 as $key => $val2) {
							echo "<td>$val2</td>";
						}
					echo "</tr>";
				}
			echo "</table>";
		}
	}

	public static function assignArrayVal($value,$array){
		$output = "";
		if(array_key_exists($value,$array)){
			$output = $array[$value];
		}
		return $output;
	}

	public static function searhcValSemicolon($semicolon, $value)	{
		$output =  false;
		$str = explode(';', $semicolon);
		foreach ($str as $key => $v1) {
			if ($v1 == $value) {
				$output=true;
				break;
			}
		}
		return $output;
	}
	public static function selectionValidate($string, $value)
	{
		if(eFunctions::searhcValSemicolon($string,$value)){
            echo "selected";
        }
        else{
        	echo "";
        }
	}
	public static function parseArray_tosemiconon($array)	{
		$output = ""; $prefix = "";
		if(!empty($array)){
			foreach ($array as $key => $value) {
				$output .= $prefix . $value;
				$prefix = ";";
			}
		}
		return $output;		
	}
}
