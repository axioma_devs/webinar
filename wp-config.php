<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webinar');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '35E{9kh}Soa%.eYxpCV+Hi*.(f*(OIZrG|YreA@x)j4YWs+8lH<eCZR7GeCA7sGI');
define('SECURE_AUTH_KEY',  'auG}CT6@,)ZbH;Z+t:nLKy=;!|FCS4i=v<4Rn]1+Nl}TYi>{^yr,$lao[3Ln>$vu');
define('LOGGED_IN_KEY',    '}D89P%dgD-_aE6YZ|UG- ?p7^8V>mHYh2^lOy[Uv*Yws+[~R{!C&9FVxnu(+4Ied');
define('NONCE_KEY',        'V?KHL&DQMl(iE#SwtMZx;w#,il@U!VK.^WN?H<$L.yS,1&_u7J!Ylfm7U,?*s(y[');
define('AUTH_SALT',        'Fx16;G%?Zi4iZrz.0l^+3K}8Ef5N{4*lPO);jjX2:TSOkXNg)o|DNO9(^}9N7?`A');
define('SECURE_AUTH_SALT', 'CDgQ?g<hTCoxOd{gL~Z&k#>~vUj0IkQ;N |a&Z>g0An0%_B`}[.GwwyF/K!8$)VI');
define('LOGGED_IN_SALT',   'Ju}4>OEg1[^0WcmuE+= wjx%_ =j))Y%Q#d[HB.fG^DX1H<|XhejKrgX %/%]LZu');
define('NONCE_SALT',       'r^_]>$Ru,v1iqGmx-qW4Ua44p{hiyxJh.}McZVH&3rtsS9fHbqpcLCqgkF6/?f8_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
